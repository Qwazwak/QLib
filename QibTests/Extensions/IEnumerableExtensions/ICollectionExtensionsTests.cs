﻿using Qib.Extensions;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace QLibTests.Extensions.ICollectionExtensionsTests
{
    public class ICollectionExtensionsTests
    {
        private const double UnchangedElementsAllowedPercent = 0.15;
        private const int ShuffleSeet = 6238790;

        private class CollectionWithoutIndex<T> : ICollection<T>
        {
            private readonly List<T> Internal;
            public CollectionWithoutIndex() => Internal = new();
            public CollectionWithoutIndex(IEnumerable<T> Items) => Internal = new(Items);
            public int Count => Internal.Count;
            public bool IsReadOnly => false;
            public void Add(T item) => Internal.Add(item);
            public void Clear() => Internal.Clear();
            public bool Contains(T item) => Internal.Contains(item);
            public void CopyTo(T[] array, int arrayIndex) => Internal.CopyTo(array, arrayIndex);
            public IEnumerator<T> GetEnumerator() => Internal.GetEnumerator();
            public bool Remove(T item) => Internal.Remove(item);
            IEnumerator IEnumerable.GetEnumerator() => ((IEnumerable)Internal).GetEnumerator();
        }
        [Theory]
        [ClassData(typeof(EnumerableToolsTests_DomainData))]
        public void ShuffleCollection(ICollection<object> Others)
        {
            object[] Copy = Others.AsEnumerable().ToArray();

            Others.Shuffle(ShuffleSeet);
            double SharedAmount = Copy.Zip(Others).Count(p => p.First == p.Second) / (double)Others.Count;
            Assert.Equal(Copy.Length, Others.Count);
            Assert.InRange(SharedAmount, 0.0, UnchangedElementsAllowedPercent);
        }
        /*
        [Theory]
        [ClassData(typeof(EnumerableToolsTests_DomainData))]
        public void AddRangeToCollection<T>(ICollection<T> Container, IEnumerable<T> Others)
        {
            Assert.Empty(Container);
            Assert.NotEmpty(Others);
            Container.AddRange(Others);
            Assert.NotEmpty(Container);
            Assert.Equal(Container.Count, Others.ParallelCount());
        }*/

        public class EnumerableToolsTests_DomainData : TheoryData<ICollection<object>>
        {
            public EnumerableToolsTests_DomainData()
            {
                foreach (ICollection<object> Group in ICollectionExtensionsTestHelpers.IdentityTransformsPowersetInvoke(new List<ICollection<object>>()
                {
                    Enumerable.Range(0, 500).Cast<object>().ToList(),
                    new string[] { "Hello", "world", "RegExr", "was", "created", "by", "gskinner", "com", "and", "is", "proudly", "hosted", "by", "Media", "Temple", "Edit", "the", "Expression", "Text", "to", "see", "matches", "Roll", "over", "matches", "or", "the", "expression", "for", "details", "PCRE", "JavaScript", "flavors", "of", "RegEx", "are", "supported", "Validate", "your", "expression", "with", "Tests", "mode", "The", "side", "bar", "includes", "a", "Cheatsheet", "full", "Reference", "and", "Help", "You", "can", "also", "Save", "Share", "with", "the", "Community", "and", "view", "patterns", "you", "create", "or", "favorite", "in", "My", "Patterns", "Explore", "results", "with", "the", "Tools", "below", "Replace", "List", "output", "custom", "results", "Details", "lists", "capture", "groups", "Explain", "describes", "your", "expression", "in", "plain", "English" }.Cast<object>().ToArray()
                }).SelectMany(i => i))
                {
                        Add(Group);
                }
            }
        }
    }
}