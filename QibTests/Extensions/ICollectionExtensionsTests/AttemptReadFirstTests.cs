﻿using Qib;
using Qib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace QLibTests.StandardImplementation
{
    public class AttemptReadFirstTests
    {
        [Fact]
        public void NoResultsYieldNull() => Assert.Null(Enumerable.Empty<int?>().FirstOrNull());

        [Fact]
        public void SameResultsRepeatCallsIntQueryEmpty()
        {
            IEnumerable<int> BaseEnumerable = Enumerable.Empty<int>();
            Assert.Equal(BaseEnumerable.FirstOrNull(), BaseEnumerable.FirstOrNull());
        }
        [Fact]
        public void SameResultsRepeatCallsIntQuery()
        {
            IEnumerable<int> SomeValues = new int[] { 1, 2, 3 };
            Assert.Equal(SomeValues.FirstOrNull(), SomeValues.FirstOrNull());
        }
    }
}