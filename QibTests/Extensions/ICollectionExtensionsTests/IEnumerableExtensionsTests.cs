﻿using Qib.Extensions;
using System.Linq;
using Xunit;

namespace QLibTests.StandardImplementation
{
    public class IEnumerableExtensionsTests
    {
        private const int LongEnoughCount = 1500000;
        [Fact]
        public void RepeatLongEnough() => Assert.Equal(LongEnoughCount, new int[] { 0, 1, 2, 3, 4 }.Repeat().Take(LongEnoughCount).ParallelCount());

        [Fact]
        public void CountParallel() => Assert.Equal(LongEnoughCount, new int[] { 0, 1, 2, 3, 4 }.Repeat().Take(LongEnoughCount).ParallelCount());
    }
}