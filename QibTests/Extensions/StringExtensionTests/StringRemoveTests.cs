using Qib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace QLibTests.Extensions.StringExtensions
{
    public class StringRemoveTests
    {
        [Theory]
        [InlineData("TestWord", "38654624TestWord1642612621+6T est Wo rd516516511301249TestWord87441165191/*162265")]
        [InlineData("ground", "Strip steak pork short loin burgdoggen fatback. Shankle meatball spare ribs boudin ham hock corned beef flank short loin chicken ground round pastrami venison.Meatloaf pastrami prosciutto, strip steak shankle biltong t - bone hamburger pig tenderloin buffalo chuck leberkas sirloin.Leberkas hamburger shank, jowl capicola fatback meatball tongue brisket tenderloin porchetta chicken biltong meatloaf corned beef.Tri - tip pancetta brisket pig prosciutto. Prosciutto hamburger picanha, beef ribs corned beef cow ground round turkey pastrami.")]
        [InlineData(". ", "Leberkas beef pork chop fatback, pastrami swine ham hock. Alcatra beef spare ribs brisket, jowl pig andouille frankfurter flank buffalo pork salami pork chop. Tongue frankfurter ground round, beef landjaeger leberkas tenderloin venison strip steak. Cow rump bresaola kielbasa ham hock doner picanha pork loin chicken beef alcatra tenderloin shoulder short ribs corned beef.")]
        public void RemoveString(string Key, string Content)
        {
            Assert.Contains(Key, Content);
            Assert.NotEqual(Content, Content.Replace(Key, string.Empty));
            Assert.DoesNotContain(Key, Content.RemoveString(Key));
        }

        [Theory]
        [InlineData('3', "38654624TestWord1642612621+6T est Wo rd516516511301249TestWord87441165191/*162265")]
        [InlineData('2', "38654624TestWord1642612621+6T est Wo rd516516511301249TestWord87441165191/*162265")]
        [InlineData('e', "Strip steak pork short loin burgdoggen fatback. Shankle meatball spare ribs boudin ham hock corned beef flank short loin chicken ground round pastrami venison.Meatloaf pastrami prosciutto, strip steak shankle biltong t - bone hamburger pig tenderloin buffalo chuck leberkas sirloin.Leberkas hamburger shank, jowl capicola fatback meatball tongue brisket tenderloin porchetta chicken biltong meatloaf corned beef.Tri - tip pancetta brisket pig prosciutto. Prosciutto hamburger picanha, beef ribs corned beef cow ground round turkey pastrami.")]
        [InlineData(' ', "Leberkas beef pork chop fatback, pastrami swine ham hock. Alcatra beef spare ribs brisket, jowl pig andouille frankfurter flank buffalo pork salami pork chop. Tongue frankfurter ground round, beef landjaeger leberkas tenderloin venison strip steak. Cow rump bresaola kielbasa ham hock doner picanha pork loin chicken beef alcatra tenderloin shoulder short ribs corned beef.")]
        public void RemoveChar(char Key, string Content)
        {
            Assert.Contains(Key, Content);
            Assert.NotEqual(Content, string.Concat(Content.Where(c => Key != c)));
            Assert.DoesNotContain(Key, Content.RemoveChars(Key));
        }
    }
}