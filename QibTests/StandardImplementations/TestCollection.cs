﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace QLibTests.Extensions.IEnumerableExtensions
{
    internal class TestCollection<T> : ICollection//, IEnumerable<T>
    {
        public ICollection<T> Items;
        public int CountTouched = 0;

        public TestCollection(ICollection<T> items) => Items = items;
        public TestCollection(IEnumerable<T> items) => Items = items.ToList();
        public TestCollection(params T[] items) => Items = items;

        public virtual int Count { get { CountTouched++; return Items.Count; } }
        public bool IsSynchronized => false;
        public object SyncRoot => this;
        public void CopyTo(Array array, int index) => throw new NotImplementedException();

        //public IEnumerator<T> GetEnumerator() => ((IEnumerable<T>)Items).GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => Items.GetEnumerator();
    }
}