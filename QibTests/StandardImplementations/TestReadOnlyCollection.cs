﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace QLibTests.Extensions.IEnumerableExtensions
{
    internal class TestReadOnlyCollection<T> : IReadOnlyCollection<T>
    {
        internal T[] Items;
        internal int CountTouched = 0;
        internal TestReadOnlyCollection(IEnumerable<T> Items) => this.Items = Items.ToArray();

        public int Count { get { CountTouched++; return Items.Length; } }
        public IEnumerator<T> GetEnumerator() => ((IEnumerable<T>)Items).GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => Items.GetEnumerator();
    }
}