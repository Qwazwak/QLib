﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace QLibTests.Extensions.IEnumerableExtensions
{
    internal class TestGenericCollection<T> : ICollection<T>
    {
        public int CountTouched = 0;
        public int CopyToTouched = 0;
        public ICollection<T> Items;
        public TestGenericCollection(ICollection<T> Items) => this.Items = Items;
        public TestGenericCollection(IEnumerable<T> Items) => this.Items = Items.ToList();
        public TestGenericCollection(params T[] Items) => this.Items = Items;

        public virtual int Count { get { CountTouched++; return Items.Count; } }
        public bool IsReadOnly => false;
        public void Add(T item) => throw new NotSupportedException("Tests must not modify the testing data");
        public void Clear() => throw new NotSupportedException("Tests must not modify the testing data");
        public bool Contains(T item) => Items.Contains(item);
        public bool Remove(T item) => throw new NotSupportedException("Tests must not modify the testing data");
        public void CopyTo(T[] array, int arrayIndex) { CopyToTouched++; Items.CopyTo(array, arrayIndex); }
        public IEnumerator<T> GetEnumerator() => Items.GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => Items.GetEnumerator();
    }
}