﻿using Qib.Extensions;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Qib
{
    public static class EnumAttributes
    {
        private static readonly ConcurrentDictionary<Type, ConcurrentDictionary<Enum, Attribute[]>> Cache = new();

        private static IReadOnlyDictionary<Enum, Attribute[]> InternalRead(Type EnumType) => Cache.GetOrAdd(EnumType, k =>
        {
            ConcurrentDictionary<Enum, Attribute[]> InnerDict = new();
            MemberInfo[] Members = k.GetMembers();
            Parallel.ForEach((Enum[])Enum.GetValues(k), EnumValue =>
            {
                MemberInfo ThisMember = Array.Find(Members, m => m.Name == EnumValue.ToString())!;
                InnerDict.TryAdd(EnumValue, (Attribute[])ThisMember.GetCustomAttributes(true));
            });
            return InnerDict;
        });

        public static IReadOnlyDictionary<TEnum, Attribute[]> Read<TEnum>() where TEnum : struct, Enum => (IReadOnlyDictionary<TEnum, Attribute[]>)InternalRead(typeof(TEnum));
        public static IReadOnlyDictionary<Enum, Attribute[]> Read(Type EnumType) => EnumType.IsEnum ? InternalRead(EnumType) : throw new ArgumentException();

        public static Attribute[] Read<TEnum>(TEnum Value) where TEnum : struct, Enum => InternalRead(typeof(TEnum))[Value];

        public static IEnumerable<Attribute> GetEnumAttributes<TEnum>(this TEnum enumValue) where TEnum : struct, Enum => Read(enumValue);
        public static IEnumerable<TAttribute> GetEnumAttributes<TEnum, TAttribute>(this TEnum enumValue) where TEnum : struct, Enum where TAttribute : Attribute => Read(enumValue).OfType<TAttribute>();

        public static bool TryGetEnumAttribute<TEnum, TAttribute>(this TEnum enumValue, out TAttribute Result) where TEnum : struct, Enum where TAttribute : Attribute => enumValue.GetEnumAttributes<TEnum, TAttribute>().TryFirst(out Result);

        public static TAttribute GetEnumAttribute<TEnum, TAttribute>(this TEnum enumValue) where TEnum : struct, Enum where TAttribute : Attribute =>
            enumValue.TryGetEnumAttribute(out TAttribute Result) ? Result : throw new InvalidOperationException();

        public static bool HasEnumAttribute<TEnum, TAttribute>(this TEnum enumValue) where TEnum : struct, Enum where TAttribute : Attribute => enumValue.GetEnumAttributes<TEnum, TAttribute>().Any();
    }
}