﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Qib.Extensions
{
    public static class ICollectionExtensions
    {
        public static void AddRange<T>(this ICollection<T> Container, IEnumerable<T> Values)
        {
            switch (Container)
            {
                case List<T> List:
                    List.AddRange(Values);
                    break;
                case ConcurrentBag<T> Bag:
                    Parallel.ForEach(Values, v => Bag.Add(v));
                    break;
                case IDictionary<object, object>:
                    throw new NotSupportedException("To add a range of values to a dictionary, you must use the dictionary specific extension");
                default:
                    foreach (T item in Values)
                        Container.Add(item);
                    break;
            }
        }

        public static Task AddRange<T>(this ICollection<T> Container, IAsyncEnumerable<T> Values, CancellationToken cancellationToken = default) => Container switch
        {
            ConcurrentBag<T> Bag => Parallel.ForEachAsync(Values, cancellationToken, (v, _) => { Bag.Add(v); return ValueTask.CompletedTask; }),
            IDictionary<object, object> => throw new NotSupportedException("To add a range of values to a dictionary, you must use the dictionary specific extension"),
            _ => Values.ForEachAsync(v => Container.Add(v), cancellationToken)
        };

        public static void Shuffle<T>(this ICollection<T> Container) => Container.Shuffle(DateTime.UtcNow.Millisecond * DateTime.UtcNow.Second);
        public static void Shuffle<T>(this ICollection<T> Container, int Seed) => Container.Shuffle(new Random(Seed));
        public static void Shuffle<T>(this ICollection<T> Container, Random rng)
        {   if (Container.Count > 0)
            {
                if (Container is IList<T> List)
                {
                    int RemainingItems = List.Count;
                    while (RemainingItems-- > 1)
                    {
                        int k = rng.Next(RemainingItems + 1);
                        (List[RemainingItems], List[k]) = (List[k], List[RemainingItems]);
                    }
                }
                else
                {
                    foreach (T i in Container.AsEnumerable().OrderBy(_ => rng.Next()))
                    {
                        Container.Remove(i);
                        Container.Add(i);
                    }
                }
            }
        }
    }
}