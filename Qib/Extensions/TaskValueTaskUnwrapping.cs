﻿using System.Threading.Tasks;

namespace Qib.Extensions
{
    public static class TaskValueTaskUnwrapping
    {
        public static Task<TResult> Unwrap<TResult>(this Task<ValueTask<TResult>> Source) => Source.ContinueWith(T => T.Result.IsCompleted ? Task.FromResult(T.Result.Result) : T.Result.AsTask().ContinueWith(I => I.Result)).Unwrap();
        public static ValueTask<TResult> Unwrap<TResult>(this ValueTask<ValueTask<TResult>> Source) => Source.IsCompleted ? Source.Result : new(Source.AsTask().Unwrap());
        public static Task<TResult> Unwrap<TResult>(this ValueTask<Task<TResult>> Source) => Source.IsCompleted ? Source.Result : Source.AsTask().Unwrap();
    }
}