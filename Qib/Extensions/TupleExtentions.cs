﻿using System.Collections.Generic;
using System.Linq;

namespace Qib.Extensions
{
    public static class TupleExtensions
    {
        public static IEnumerable<KeyValuePair<TKey, TValue>> ToKeyValuePairs<TKey, TValue>(this IEnumerable<(TKey Key, TValue Value)> SourcePairs) => SourcePairs.Select(v => KeyValuePair.Create(v.Key, v.Value));
    }
}