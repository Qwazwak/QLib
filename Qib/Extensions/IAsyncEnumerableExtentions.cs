﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Qib.Extensions
{
    public static class IAsyncEnumerableExtentions
    {
        public static IAsyncEnumerable<TResult> SelectAsync<TResult>(this IAsyncEnumerable<ValueTask<TResult>> Source) => Source.SelectAwait(i => i);
        public static IAsyncEnumerable<TResult> SelectAsync<TResult>(this IAsyncEnumerable<Task<TResult>> Source) => Source.SelectAwait(i => new ValueTask<TResult>(i));
    }
}