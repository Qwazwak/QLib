﻿using System.Collections.Generic;
using System.Linq;

namespace Qib.Extensions
{
    public static class GenericExtensions
    {
        public static bool EqualsAny<T>(this T Base, params T[] Canidates) => EqualsAny(Base, EqualityComparer<T>.Default, Canidates);
        public static bool EqualsAny<T>(this T Base, EqualityComparer<T> EqComp, params T[] Canidates) => Canidates.Contains(Base, EqComp);
    }
}