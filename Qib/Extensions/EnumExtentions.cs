﻿using System;
using System.Collections.Generic;

namespace Qib.Extensions
{
    public static class EnumExtensions
    {
        public static IEnumerable<(TSource, TEnum)> ModWithEnum<TSource, TEnum>(this IEnumerable<TSource> Enumerable) where TEnum : struct, Enum => Enumerable.ModWith(Enum.GetValues<TEnum>());
    }
}