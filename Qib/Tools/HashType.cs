﻿namespace Qib.Tools
{
    /// <summary>
    ///     The type of hashing function
    /// </summary>
    public enum HashType
    {
        SHA1, // 160 bit
        SHA256, // 256 bit
        SHA384, // 384 bit
        SHA512 // 512 bit
    }
}
