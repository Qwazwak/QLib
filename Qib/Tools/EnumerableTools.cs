﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Qib.Tools
{
    public static class EnumerableTools
    {
        public static IEnumerable<int> Domain(int StartValue, int IncreaseByValue, int MaximumValueExclusive)
        {
            for (int i = StartValue; i < MaximumValueExclusive; i += IncreaseByValue)
                yield return i;
        }

        public static IEnumerable<TSource> ToEnumerable<TSource>(params TSource[] source) => source == null || source.Length < 1 ? Enumerable.Empty<TSource>() : source;

        public static IEnumerable<TSource> Repeat<TSource>(Func<TSource> ItemGenerator, int TimesToRepeat) => Repeat(ItemGenerator).Take(TimesToRepeat);

        public static IEnumerable<TSource> Repeat<TSource>(Func<TSource> ItemGenerator)
        {
            while (true)
                yield return ItemGenerator.Invoke();
        }
    }
}
