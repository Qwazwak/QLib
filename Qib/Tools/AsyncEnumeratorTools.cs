﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Qib.Tools
{
    public static class AsyncEnumeratorTools
    {
        public static IAsyncEnumerable<TResult> Repeat<TResult>(Func<Task<TResult>> ItemGenerator, int TimesToRepeat) => Repeat(ItemGenerator).Take(TimesToRepeat);
        public static IAsyncEnumerable<TResult> Repeat<TResult>(Func<ValueTask<TResult>> ItemGenerator, int TimesToRepeat) => Repeat(ItemGenerator).Take(TimesToRepeat);

        public static async IAsyncEnumerable<TResult> Repeat<TResult>(Func<Task<TResult>> ItemGenerator)
        {
            while (true)
                yield return await ItemGenerator.Invoke();
        }

        public static async IAsyncEnumerable<TResult> Repeat<TResult>(Func<ValueTask<TResult>> ItemGenerator)
        {
            while (true)
                yield return await ItemGenerator.Invoke();
        }
    }
}
