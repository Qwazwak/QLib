﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Threading;
using System.Threading.Tasks;

namespace Qib.Tools
{
    public class RSA_PrivateKey : RSA_PublicKey
    {
        private RSAParameters RSA;

        public ReadOnlySpan<byte> P => RSA.P;
        public ReadOnlySpan<byte> Q => RSA.Q;
        public ReadOnlySpan<byte> DP => RSA.DP;
        public ReadOnlySpan<byte> DQ => RSA.DQ;
        public ReadOnlySpan<byte> InverseQ => RSA.InverseQ;
        public ReadOnlySpan<byte> D => RSA.D;

        public static RSA_PrivateKey GenerateNew(int KeySize)
        {
            using RSACryptoServiceProvider rsa = new(KeySize);
            rsa.PersistKeyInCsp = false;
            RSAParameters PrivateKey = rsa.ExportParameters(true);
            return new RSA_PrivateKey(KeySize, PrivateKey.Exponent!, PrivateKey.Modulus!, PrivateKey.P!, PrivateKey.Q!, PrivateKey.DP!, PrivateKey.DQ!, PrivateKey.InverseQ!, PrivateKey.D!);
        }

        public RSA_PrivateKey(RSA_PrivateKey Other) : this(Other.KeySize, Other.Exponent, Other.Modulus,
             Other.P, Other.Q, Other.DP, Other.DQ, Other.InverseQ, Other.D)
        {
        }

        public RSA_PrivateKey(int KeySize, ReadOnlySpan<byte> Exponent, ReadOnlySpan<byte> Modulus,
            ReadOnlySpan<byte> P, ReadOnlySpan<byte> Q, ReadOnlySpan<byte> DP, ReadOnlySpan<byte> DQ, ReadOnlySpan<byte> InverseQ, ReadOnlySpan<byte> D)
            : this(KeySize, Exponent.ToArray(), Modulus.ToArray(), P.ToArray(), Q.ToArray(), DP.ToArray(), DQ.ToArray(), InverseQ.ToArray(), D.ToArray()) { }

        public RSA_PrivateKey(int KeySize, byte[] Exponent, byte[] Modulus,
             byte[] P, byte[] Q, byte[] DP, byte[] DQ, byte[] InverseQ, byte[] D) : base (KeySize, Exponent, Modulus)
        {
            RSA = new RSAParameters() { Exponent = Exponent, Modulus = Modulus, P = P, Q = Q, DP = DP, DQ = DQ, InverseQ = InverseQ, D = D, };
        }

        public byte[] SignData(byte[] OriginalData, HashType HashAlgorithm) => SignHash(HashCalculator.Create(HashAlgorithm, OriginalData), HashAlgorithm);

        public Task<byte[]> SignDataAsync(Stream OriginalData, HashType HashAlgorithm, CancellationToken cancellationToken = default) => HashCalculator.CreateAsync(HashAlgorithm, OriginalData, cancellationToken).ContinueWith(t => SignHash(t.Result, HashAlgorithm));
        public Task<byte[]> SignDataAsync(byte[] OriginalData, HashType HashAlgorithm, CancellationToken cancellationToken = default) => HashCalculator.CreateAsync(HashAlgorithm, OriginalData, cancellationToken).ContinueWith(t => SignHash(t.Result, HashAlgorithm));

        public byte[] SignHash(byte[] DataHash, HashType HashAlgorithm)
        {
            using RSACryptoServiceProvider rsa = new(KeySize);
            rsa.PersistKeyInCsp = false;
            rsa.ImportParameters(RSA);

            RSAPKCS1SignatureFormatter rsaFormatter = new(rsa);
            rsaFormatter.SetHashAlgorithm(HashTypeToString(HashAlgorithm));
            return rsaFormatter.CreateSignature(DataHash);
        }
    }
}
